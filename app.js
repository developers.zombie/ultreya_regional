const express = require('express');
const bodyParser = require("body-parser");
const app = express();
const route = require("./src/routers/");
const mongoose = require('mongoose');
const cors = require('cors');
//const confi = require('./config');
const path = require('path');
require("dotenv").config();
let port = process.env.PORT || 3000;
let db = `mongodb://${process.env.DB_USER}:${process.env.DB_PASS}@ds123645.mlab.com:23645/mcc_ultrella`;
mongoose.set('useCreateIndex', true);
mongoose.connect(db, {
    useNewUrlParser: true
}, function (err) {
    if (err) throw err;
    console.log('Successfully connected to MongoDB');
});
app.use(cors());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json({
    limit: "300kb"
}));
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname + '/public')));
app.use(route);
app.listen(process.env.PORT || 3000, () => console.log(`Serve run dev on http://localhost:${port}`));