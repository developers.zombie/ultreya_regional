const path = require('path');
const img = path.join(__dirname + '/logo.jpg');
let items = '';

function Mays(string) {
    //return string.charAt(0).toUpperCase() + string.slice(1);
    return string.toUpperCase();
}

function MaysPrimera(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
    //return string.toUpperCase();
}
exports.template_listado = (data, fecha) => {
    for (let i = 0; i < data.length; i++) {
        items += `
             <tr style="padding:10px; ">
              <th scope="row">${i+1}</th>
              <td style="padding-left:5px;">${Mays(data[i].nombre)}  ${Mays(data[i].apellido)}</td>
              <td style="padding-left:5px;">${data[i].telefono}</td>
              <td style="padding-left:5px;">${data[i].correo}</td>
              <td style="padding-left:5px;">${MaysPrimera(data[i].diocesis)}</td>
            </tr>`;
    }
    return `
<!doctype html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <style>
    body{
      font-family: "Times New Roman", Times, serif;
      
    }
     table{ 
      width: 100%;
      font-size: 10px;
      margin-top: 30px;
      line-height: 2;
      
     }
    </style>
  </head>
  <body>
  <center> 

    <img src="http://ultreyadesolteros.herokuapp.com/img/mcc.png"   width="92" height="92"/>

     <p>Ultreya Regional de Solteros</p>
    <p>Listado de Participantes</p>
  </center>
  <div >
      <table border=1 cellpadding="0" cellspacing="0">
        
        <tr style="background: #ddd; color:#000; ">
          <th scope="col">#</th>
          <th >NOMBRES Y APELLIDOS</th>
          <th >TELÉFONO</th>
          <th >CORREO</th>
          <th >DIÓCESIS</th>
        </tr>
            
        ${items}
      </table>
     </div>
 
  </body>

  </html>`;
};