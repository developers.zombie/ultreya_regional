const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Model = new Schema({
    nombre: {
        type: String,
        lowercase: true
    },
    apellido: {
        type: String,
        lowercase: true
    },
    correo: {
        type: String,
        lowercase: true,
        unique: true
    },
    telefono: {
        type: String
    },
    diocesis: {
        type: String,
        lowercase: true
    }
}, {
    timestamps: true
});
module.exports = mongoose.model("Model", Model);