const express = require("express");
const route = express.Router();
const AppCtrl = require("../controllers/app");
const frotendCtrl = require("../controllers/frontend");
//frotend
route.get("/", frotendCtrl.index);
route.get("/listado", frotendCtrl.listado);
route.get("/admin", frotendCtrl.participante);
//cliente
route.get("/resgistros", AppCtrl.index);
route.get("/registro/:ci", AppCtrl.show);
route.post("/registro", AppCtrl.store);
route.put("/registro/:id", AppCtrl.update);
route.delete("/registro/:id", AppCtrl.delete);
module.exports = route;
//usuario: gregoryj733 clave: super17020751