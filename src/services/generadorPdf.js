const moment = require("moment");
const pdf = require("html-pdf");
const path = require("path");
require("dotenv").config();
moment.locale('es');
const fs = require("fs");
const options = {
    format: "Letter",
    orientation: "landscape", // portrait or landscape
    border: {
        top: "20px", // default is 0, units: mm, in, in, px
        right: "20px",
        bottom: "20px",
        left: "25px"
    },
    header: {
        height: ""
    },
    footer: {
        height: "15px",
        contents: "<span style='font-size: 10px;'>Diócesis de Carúpano, " + moment().format('LLL') + "</span>"
    }
};
module.exports.generadorPdf = template => {
    return new Promise((resolve, reject) => {
        pdf.create(template, options).toFile(`${process.env.URL_TEMPLATE}listado.pdf`, function (err, res) {
            if (err) {
                reject(err);
            }
            resolve(`${process.env.URL_TEMPLATE}listado.pdf`);
        });
    });
};