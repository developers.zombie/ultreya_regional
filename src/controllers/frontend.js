const fs = require('fs');
const path = require('path');
const pdf = require("../services/generadorPdf");
const tem = require("../template");
const AppController = require('../models/model');
const moment = require("moment");
moment.locale("es");
module.exports = {
    index(req, res) {
        let fecha1 = moment();
        let fechaFinal = moment('2019-03-11');
        let diasRestantes = fechaFinal.diff(fecha1, 'days');
        if (diasRestantes == 0) {
            res.render('block');
        }
        res.render('index', {
            diasRestantes
        });
    },
    listado(req, res) {
        //res.sendFile(path.join(__dirname + '/listado'));pdf
        AppController.find().then(clientes => {
            pdf.generadorPdf(tem.template_listado(clientes, moment().format("LL"))).then(response => {
                console.log(response);
                let file = response;
                res.download('src/template/listado.pdf');
                //res.status(200).download('src/template/listado.pdf', err => console.log(err));
            }).catch(error => console.log(error));
        }).catch(error => console.log(error));
    },
    participante(req, res) {
        AppController.find().then(data => {
            res.render('listado', {
                data
            });
        }).catch(error => console.log(error));
    }
}