const AppController = require('../models/model');
const service = require('../services/sendEmail');
module.exports = {
    index(req, res) {
        AppController.find().then(clientes => {
            res.status(200).json(clientes);
        }).catch(error => console.log(error));
    },
    show(req, res) {
        AppController.find({
            rut: req.params.rut
        }).then(cliente => {
            res.status(200).json(cliente);
        }).catch(error => console.log(error));
    },
    store(req, res) {
        let cliente = new AppController(req.body);
        cliente.save().then(cliente => {
            //aqui se realiza el envio del email
            let nomApe = req.body.nombre + ' ' + req.body.apellido;
            let email = req.body.correo;
            console.log(email);
            service.sendEmail(email, '<h1>hola</h1>');
            res.status(201).json(cliente);
        }).catch(error => res.status(500).json(error));
    },
    update(req, res) {
        AppController.findByIdAndUpdate(req.params.id, req.body, (err, cliente) => {
            if (err) {
                return res.status(500).json(err);
            }
            return res.status(200).json(cliente);
        });
    },
    delete(req, res) {
        AppController.deleteOne({
            _id: req.params.id
        }).then(cliente => {
            res.status(200).json(cliente);
        }).catch(error => console.log(error));
    }
}
//genaimar montaño    04128331473     genaimar@gmail.com  carúpano
//yiceidis garcía     04262896424     yiceidisg@gmail.com     carúpano