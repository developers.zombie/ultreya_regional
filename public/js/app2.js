/*
function getListados() {
    document.getElementById('spinner').style.display = 'block';
    axios.get('/resgistros').then(function (response) {
        render(response.data);
        document.getElementById('spinner').style.display = 'none';
    }).catch(function (error) {
        // handle error
        console.log(error);
    });
}

function render(data) {
    let table_body = document.getElementById('body-table');
    data.forEach(function (elem, i) {
        //console.log(elem.ci);
        return table_body.innerHTML += `
        <tr>
            <th scope="row">${i+1}</th>
            <td>${elem.nombre}  ${elem.apellido}</td>
            <td>${elem.telefono}</td>
            <td>${elem.correo}</td>
            <td>${elem.diocesis}</td>
            <td>
              <button class="btn btn-sm btn-danger delete" 
              type="button"  value="${elem._id}" onclick="deleteCliente(this);">
                  X
              </button>

            </td>
          </tr>
    `;
    });
}
*/
function getListadoPDF() {
    axios.get('/listado').then(function (response) {
        swal({
            title: "Descarga Iniciada!",
            text: "El listado sera descargado en formato PDF",
            icon: "success",
            button: "Ok!",
        });
        console.log(response);
        //location.href = "./";
    }).catch(function (error) {
        // handle error
        console.log(error);
    });
}

function deleteCliente(btn) {
    axios.delete('/registro/' + btn.value).then(function (response) {
        // handle success
        swal({
            title: "Registro Eliminado!",
            text: "...",
            icon: "success",
            button: "Ok!",
        }).then(function () {
            window.location = "./admin";
        });
        //location.href = "./admin";
    }).catch(function (error) {
        // handle error
        console.log(error);
    });
}
//bt.addEventListener('click',deleteCliente);
//window.addEventListener("load", getListados);