//toda la logica para la conexion al servidor
//listar todos los clientes
let btn_get_cliente = document.querySelector('#btn-button');

function getListados() {
    axios.get('/resgistros').then(function (response) {
        // handle success
        //document.querySelector('#rut').value = "";
        //render(response.data);
        console.log(response);
    }).catch(function (error) {
        // handle error
        console.log(error);
    });
}

function form_valid(event) {
    var form = document.getElementById("form-signin");
    event.preventDefault();
    var valid = false;
    for (var i = 0; i < (form.length - 2); i++) {
        if (form.elements[i].value.length > 0) {
            valid = true;
        } else {
            valid = false;
        }
    }
    //todo correcto
    if (valid) {
        storeCliente(); //se guarda los datos
    } else {
        //alert('Completa los Campos!');
        swal({
            title: "Completa los Campos!",
            text: "Todos los campos son requeridos",
            icon: "warning",
            button: "Ok",
        });
    }
}

function storeCliente() {
    axios({
        method: 'post',
        url: '/registro/',
        data: {
            nombre: document.querySelector("#nombre").value,
            apellido: document.querySelector("#apellido").value,
            correo: document.querySelector("#correo").value,
            telefono: document.querySelector("#telefono").value,
            diocesis: document.querySelector("#diocesis").value
        }
    }).then(function (response) {
        //alert('Cliente Guardado!');
        swal({
            title: "Inscripción Completada!",
            text: "Ya estas participando",
            icon: "success",
            button: "Ok!",
        }).then(function () {
            window.location = "./";
        });
        //console.log(response);
        document.querySelector("#nombre").value = "";
        document.querySelector("#apellido").value = "";
        document.querySelector("#correo").value = "";
        document.querySelector("#telefono").value = "";
        document.querySelector("#diocesis").value = "";
        //location.href = "/";
    }).catch(function (error) {
        // handle error
        swal({
            title: "Ya te encuentras registrado!",
            text: "Tu correo ya se encuentras registrado y no puede ser usado de nuevo",
            icon: "warning",
            button: "Ok",
        });
        console.log(error);
    });
}
/*
function updateCliente(){
    axios({
      method: 'put',
      url: '/cliente/' + document.querySelector('#update').value ,
      data: {
           nroOper: document.querySelector("#input_nop").value,
           rut: document.querySelector("#input_rt").value,
           nombre_cliente :document.querySelector("#input_nc").value,
           financiado: document.querySelector("#input_sf").value,
           tasaOp : document.querySelector("#input_to").value,
           codEjecutivo : document.querySelector("#input_ce").value,
           nombreEjecutivo :document.querySelector("#input_ne").value,
           sucursal: document.querySelector("#input_sc").value
      }
    })
    .then(function (response) {

        alert('Cliente Actulizado!');
        location.href ="http://localhost:3000";

    })
    .catch(function (error) {
        // handle error
        console.log(error);
    });
}*/
function redirec() {
    let pass = prompt('¿Cúal es la Contraseña?');
    if (pass === "zamuro") {
        location.href = "/listado.html";
    } else {
        location.href = "/";
    }
}
btn_get_cliente.addEventListener('click', form_valid);
//